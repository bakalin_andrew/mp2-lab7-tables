#include <gtest/gtest.h>

#include "include/TScanTable.h"
#include "include/TWord.h"

TEST(BakalinKazakov_TScanTable, Can_Create_Scan_Table) {
  ASSERT_NO_THROW(TScanTable scan_table());
}

TEST(BakalinKazakov_TScanTable, Cant_Create_Scan_Table_With_Negative_Size) {
  ASSERT_ANY_THROW(TScanTable scan_table(-14));
}

TEST(BakalinKazakov_TScanTable, Can_Insert_Record) {
  TScanTable scan_table(5);
  TWord *word = new TWord("Multiprogramm");

  ASSERT_NO_THROW(scan_table.InsRecord("ab", word));
}

TEST(BakalinKazakov_TScanTable, IsFull_Works_Correctly) {
  TScanTable scan_table(3);
  TWord *word = new TWord("Multiprogramm");

  scan_table.InsRecord("aa", word);
  scan_table.InsRecord("ab", word);
  scan_table.InsRecord("ac", word);

  EXPECT_EQ(scan_table.IsFull(), true);
}

TEST(BakalinKazakov_TScanTable, Can_Get_Table_Size) {
  TScanTable scan_table(3);

  EXPECT_EQ(scan_table.GetTableSize(), 3);
}

TEST(BakalinKazakov_TScanTable, Can_Get_Current_Position) {
  TScanTable scan_table(3);

  EXPECT_EQ(scan_table.GetCurrentPos(), 0);
}

TEST(BakalinKazakov_TScanTable, Can_Set_Current_Position) {
  TScanTable scan_table(3);
  TWord *word = new TWord("Multiprogramm");
  scan_table.InsRecord("aa", word);
  scan_table.InsRecord("ab", word);

  scan_table.SetCurrentPos(1);

  EXPECT_EQ(scan_table.GetCurrentPos(), 1);
}

TEST(BakalinKazakov_TScanTable, Cant_Set_Current_Position_If_Tab_Is_Empty){
  TScanTable scan_table(3);

  scan_table.SetCurrentPos(1);

  EXPECT_EQ(scan_table.GetCurrentPos(), 0);
}

TEST(BakalinKazakov_TScanTable, Can_Go_Next) {
  TScanTable scan_table(3);
  TWord *word = new TWord("Multiprogramm");
  scan_table.InsRecord("aa", word);

  scan_table.Reset();
  scan_table.GoNext();

  EXPECT_EQ(scan_table.GetCurrentPos(), 1);
}

TEST(BakalinKazakov_TScanTable, Can_Reset_Table) {
  TScanTable scan_table(3);

  scan_table.GoNext();
  scan_table.Reset();

  EXPECT_EQ(scan_table.GetCurrentPos(), 0);
}

TEST(BakalinKazakov_TScanTable, IsTabEnded_Works_Correctly) {
  TScanTable scan_table(3);

  scan_table.GoNext();
  scan_table.GoNext();
  scan_table.GoNext();

  EXPECT_EQ(scan_table.IsTabEnded(), true);
}

TEST(BakalinKazakov_TScanTable, Can_Find_Record) {
  TScanTable scan_table(3);
  TWord *word1 = new TWord("Multiprogramm");
  TWord *word2 = new TWord("SGBash");
  scan_table.InsRecord("aa", word1);
  scan_table.InsRecord("ab", word2);

  TWord* result = (TWord*)scan_table.FindRecord("ab");

  EXPECT_EQ(result->GetWord(), "SGBash");
}

TEST(BakalinKazakov_TScanTable, Can_Delete_Record) {
  TScanTable scan_table(3);
  TWord *word1 = new TWord("Multiprogramm");
  TWord *word2 = new TWord("SGBash");
  scan_table.InsRecord("aa", word1);
  scan_table.InsRecord("ab", word2);

  scan_table.DelRecord("ab");
  TWord* result = (TWord*)scan_table.FindRecord("ab");

  EXPECT_EQ(result, nullptr);
}
