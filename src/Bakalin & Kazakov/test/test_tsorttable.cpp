#include <gtest/gtest.h>

#include "include/TSortTable.h"
#include "include/TWord.h"

TEST(BakalinKazakov_TSortTable, Can_Create_Sort_Table) {
  ASSERT_NO_THROW(TSortTable table());
}

TEST(BakalinKazakov_TSortTable, Cant_Create_Sort_Table_With_Negative_Size) {
  ASSERT_ANY_THROW(TSortTable table(-14));
}

TEST(BakalinKazakov_TSortTable, Can_Set_Sort_Method) {
  TSortTable table(5);

  ASSERT_NO_THROW(table.SetSortMethod(TSortTable::SortMethod::INSERT_SORT));
}

TEST(BakalinKazakov_TSortTable, can_get_sort_method) {
  TSortTable table(5);

  table.SetSortMethod(TSortTable::SortMethod::INSERT_SORT);

  ASSERT_NO_THROW(table.GetSortMethod());
}

TEST(BakalinKazakov_TSortTable, get_sort_method_works_properly) {
  TSortTable table(5);

  table.SetSortMethod(TSortTable::SortMethod::INSERT_SORT);

  EXPECT_EQ(table.GetSortMethod(), TSortTable::SortMethod::INSERT_SORT);
}

TEST(BakalinKazakov_TSortTable, Can_Insert_Record) {
  TSortTable table(5);
  TWord* word = new TWord("Multiprogramm");

  ASSERT_NO_THROW(table.InsRecord("ab", word));
}

TEST(BakalinKazakov_TSortTable, IsFull_Works_Correctly) {
  TSortTable table(3);
  TWord* word = new TWord("Multiprogramm");

  table.InsRecord("aa", word);
  table.InsRecord("ab", word);
  table.InsRecord("ac", word);

  EXPECT_EQ(table.IsFull(), true);
}

TEST(BakalinKazakov_TSortTable, Can_Get_Table_Size) {
  TSortTable table(3);

  EXPECT_EQ(table.GetTableSize(), 3);
}

TEST(BakalinKazakov_TSortTable, Can_Get_Current_Position) {
  TSortTable table(3);

  EXPECT_EQ(table.GetCurrentPos(), 0);
}

TEST(BakalinKazakov_TSortTable, Can_Set_Current_Position) {
  TSortTable table(3);
  TWord* word = new TWord("Multiprogramm");
  table.InsRecord("aa", word);
  table.InsRecord("ab", word);

  table.SetCurrentPos(1);

  EXPECT_EQ(table.GetCurrentPos(), 1);
}

TEST(BakalinKazakov_TSortTable, Cant_Set_Current_Position_If_Tab_Is_Empty){
  TSortTable table(3);

  table.SetCurrentPos(1);

  EXPECT_EQ(table.GetCurrentPos(), 0);
}

TEST(BakalinKazakov_TSortTable, Can_Go_Next) {
  TSortTable table(3);
  TWord* word = new TWord("Multiprogramm");
  table.InsRecord("aa", word);

  table.Reset();
  table.GoNext();

  EXPECT_EQ(table.GetCurrentPos(), 1);
}

TEST(BakalinKazakov_TSortTable, Can_Reset_Table) {
  TSortTable table(3);

  table.GoNext();
  table.Reset();

  EXPECT_EQ(table.GetCurrentPos(), 0);
}

TEST(BakalinKazakov_TSortTable, IsTabEnded_Works_Correctly) {
  TSortTable table(3);

  table.GoNext();
  table.GoNext();
  table.GoNext();

  EXPECT_EQ(table.IsTabEnded(), true);
}

TEST(BakalinKazakov_TSortTable, Can_Find_Record) {
  TSortTable table(3);
  TWord* word1 = new TWord("Multiprogramm");
  TWord* word2 = new TWord("SGBash");
  table.InsRecord("aa", word1);
  table.InsRecord("ab", word2);

  TWord* result = (TWord*)table.FindRecord("ab");

  EXPECT_EQ(result->GetWord(), "SGBash");
}

TEST(BakalinKazakov_TSortTable, Can_Delete_Record) {
  TSortTable table(3);
  TWord* word1 = new TWord("Multiprogramm");
  TWord* word2 = new TWord("SGBash");
  table.InsRecord("aa", word1);
  table.InsRecord("ab", word2);

  table.DelRecord("ab");
  TWord* result = (TWord*)table.FindRecord("ab");

  EXPECT_EQ(result, nullptr);
}
