#Лабораторная работа №7. Таблицы

##1. Введение

Представление данных во многих задачах из разных областей человеческой деятельности может быть организовано при помощи таблиц. Таблицы представляют собой последовательности строк (записей), структура строк может быть различной, но обязательным является поле, задающее имя (ключ) записи. Таблицы применяются в бухгалтерском учете (ведомости заработной платы), в торговле (прайс-листы), в образовательных учреждениях (экзаменационные ведомости) и являются одними из наиболее распространенных структур данных, используемых при создании системного и прикладного математического обеспечения. Таблицы широко применяются в трансляторах (таблицы идентификаторов) и операционных системах, могут рассматриваться как программная реализация ассоциативной памяти и т.п. Существование отношения «иметь имя» является обязательным в большинстве разрабатываемых программистами структур данных; доступ по имени в этих структурах служит для получения соответствия между адресным принципом указания элементов памяти ЭВМ и общепринятым (более удобным для человека) способом указания объектов по их именам.

Целью лабораторной работы помимо изучения способов организации таблиц является начальное знакомство с принципами проектирования структур хранения, используемых в методах решения прикладных задач. На примере таблиц изучаются возможность выбора разных вариантов структур хранения, анализ их эффективности и определения областей приложений, в которых выбираемые структуры хранения являются наиболее эффективными.

В качестве практической задачи, на примере которой будут продемонстрированы возможные способы организации таблиц, рассматривается проблема статистической обработки результатов экзаменационной успеваемости студентов (выполнение таких, например, вычислений как определение среднего балла по предмету и/или по группе при назначении студентов на стипендию или при распределении студентов по кафедрам и т.п.).

###1.1 Основные понятия и определения

**Таблица** (от лат. tabula – доска) – динамическая структура данных, базисным множеством которой является семейство линейных структур из записей (базисное отношение включения определяется операциями вставки и удаления записей).

**Запись** – кортеж, каждый элемент которого обычно именуется полем.

**Имя записи** (ключ) – одно из полей записи, по которому обычно осуществляется поиск записей в таблице; остальные поля образуют тело записи.

**Двоичное дерево поиска** – это представление данных в виде дерева, для которого выполняются условия:

- для любого узла (вершины) дерева существует не более двух потомков (двоичное дерево);
- для любого узла значения во всех узлах левого поддерева меньше значения в узле;
- для любого узла значения во всех узлах правого поддерева больше значения в узле.

**Хеш-функция**  – функция, ставящая в соответствие ключу номер записи в таблице (используется при организации таблиц с вычислимым входом).

###1.2 Требования к лабораторной работе

В рамках данной лабораторной работы ставится задача создания программных средств, поддерживающих табличные динамические структуры данных (таблицы) и базовые операции над ними:

-	поиск записи;
-	вставка записи (без дублирования);
-	удаление записи.

Выполнение операций над таблицами может осуществляться с различной степенью эффективности в зависимости от способа организации таблицы. 

В рамках лабораторной работы как показатель эффективности предлагается использовать количество операций, необходимых для выполнения операции поиска записи в таблице. Величина этого показателя должна определяться как аналитически (при использовании тех или иных упрощающих предположений), так и экспериментально на основе проведения вычислительных экспериментов.

В лабораторной работе предлагается реализовать следующие типы таблиц:

-	просмотровые (неупорядоченные);
-	упорядоченные (сортированные);
-	таблицы со структурами хранения на основе деревьев поиска;

Необходимо разработать интерфейс доступа к операциям поиска, вставки и удаления, не зависящий от способа организации таблицы.

###1.3 Структура проекта

При выполнении данной лабораторной работы следует разработать иерархию классов, учитывая, что все таблицы имеют как общие свойства (их описание следует поместить в определении базового класса), так и особенности выполнения отдельных операций (реализуются в отдельных классах для каждого вида таблиц). При разработке классов используется ранее разработанный класс **TDatValue**.
 
Рекомендуемый состав классов приведен ниже.

**TTabRecord.h**, **TTabRecord.cpp** – модуль с классом объектов-значений для записей таблицы;

**TTable.h** – абстрактный базовый класс, содержит спецификации методов таблицы;

**TArrayTable.h**, **TArrayTable.cpp** – абстрактный базовый класс для таблиц с непрерывной памятью;

**TScanTable.h**, **TScanTable.cpp** – модуль с классом, обеспечивающим реализацию просматриваемых таблиц;

**TSortTable.h**, **TSortTable.cpp** – модуль с классом, обеспечивающим реализацию упорядоченных таблиц;

**TTreeNode.h**, **TTreeNode.cpp** – модуль с абстрактным базовым классом объектов-значений для деревьев;

**TTreeTable.h**, **TTreeTable.cpp** – модуль с классом, реализующим таблицы в виде деревьев поиска;

**TBalanceNode.h**, **TBalanceNode.cpp** – модуль с базовым классом объектов-значений для сбалансированных деревьев;

**TBalanceTree.h**, **TBalanceTree.cpp** – модуль с классом, реализующим таблицы в виде сбалансированных деревьев поиска;

##2. Выполнение лабораторной работы

###2.1 Модуль с классом объектов-значений для записей таблицы

Заголовочный файл - *TTabRecord.h*

	#pragma once
	
	#include "TDatValue.h"
	
	#include <string>
	
	// Класс объектов-значений для записей таблицы
	class TTabRecord : public TDatValue {
	 public:
	  TTabRecord(std::string key = "", TDatValue* value = nullptr);
	  TTabRecord& operator=(TTabRecord&);
	
	  void SetKey(const std::string key);
	  std::string GetKey(void);
	  void SetValue(TDatValue* value);
	  TDatValue* GetValue(void);
	
	  virtual TDatValue* GetCopy(void) override;
	
	  virtual bool operator==(const TTabRecord&);
	  virtual bool operator<(const TTabRecord&);
	  virtual bool operator>(const TTabRecord&);
	
	 protected:
	  std::string key_;  // ключ записи
	  TDatValue* value_;
	
	  // Дружественные классы для различных типов таблиц
	  friend class TArrayTable;
	  friend class TScanTable;
	  friend class TSortTable;
	  friend class TTreeNode;
	  friend class TTreeTable;
	  friend class TArrayHash;
	  friend class TListHash;
	};

Реализация - *TTabRecord.cpp*

	#include "TTabRecord.h"
	
	#include <string>
	
	TTabRecord::TTabRecord(std::string key, TDatValue* value) {
	  key_ = key;
	  value_ = value;
	}
	
	TTabRecord& TTabRecord::operator=(TTabRecord& tr) {
	  key_ = tr.key_;
	  value_ = tr.value_;
	
	  return *this;
	}
	
	void TTabRecord::SetKey(const std::string key) {
	  key_ = key;
	}
	
	std::string TTabRecord::GetKey() {
	  return key_;
	}
	
	void TTabRecord::SetValue(TDatValue* value) {
	  value_ = value;
	}
	
	TDatValue* TTabRecord::GetValue() {
	  return value_;
	}
	
	TDatValue* TTabRecord::GetCopy() {
	  TDatValue* copy = new TTabRecord(key_, value_);
	
	  return copy;
	}
	
	bool TTabRecord::operator==(const TTabRecord& tr) {
	  return key_ == tr.key_;
	}
	
	bool TTabRecord::operator<(const TTabRecord& tr) {
	  return key_ < tr.key_;
	}
	
	bool TTabRecord::operator>(const TTabRecord& tr) {
	  return key_ > tr.key_;
	}

Модульное тестирование - *test_ttabrecord.cpp*

	TEST(BakalinKazakov_TTabRecord, Can_Create_Tab_Record) {
	  ASSERT_NO_THROW(TTabRecord tab_record = TTabRecord("", nullptr));
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Assign_To_Itself) {
	  TTabRecord tab_record = TTabRecord("", nullptr);
	
	  ASSERT_NO_THROW(tab_record = tab_record);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Assign_Tab_Record) {
	  TTabRecord tab_record1 = TTabRecord("1", nullptr);
	  
	  TTabRecord tab_record2 = tab_record1;
	
	  EXPECT_EQ(tab_record1, tab_record2);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Get_Key) {
	  TTabRecord tab_record = TTabRecord("1", nullptr);
	
	  EXPECT_EQ(tab_record.GetKey(), "1");
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Set_Key) {
	  TTabRecord tab_record = TTabRecord("1", nullptr);
	  
	  tab_record.SetKey("2");
	
	  EXPECT_EQ(tab_record.GetKey(), "2");
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Get_Value) {
	  TTabRecord tab_record = TTabRecord("1", nullptr);
	
	  EXPECT_EQ(tab_record.GetValue(), nullptr);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Set_Value) {
	  TTabRecord tab_record = TTabRecord("1", nullptr);
	  
	  tab_record.SetValue(&tab_record);
	
	  EXPECT_EQ(tab_record.GetValue(), &tab_record);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Get_Copy) {
	  TTabRecord tab_record1 = TTabRecord("1", nullptr);
	  
	  TDatValue* tab_record2 = tab_record1.GetCopy();
	  tab_record1.SetKey("2");
	
	  EXPECT_EQ(((TTabRecord*)tab_record2)->GetKey(), "1");
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Compare_Two_Equal_Records) {
	  TTabRecord tab_record1 = TTabRecord("1", nullptr);
	  TTabRecord tab_record2 = TTabRecord("1", nullptr);
	
	  EXPECT_EQ(tab_record1 == tab_record2, true);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Compare_Two_Different_Records) {
	  TTabRecord tab_record1 = TTabRecord("1", nullptr);
	  TTabRecord tab_record2 = TTabRecord("2", nullptr);
	
	  EXPECT_EQ(tab_record1 == tab_record2, false);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Compare_For_Less) {
	  TTabRecord tab_record1 = TTabRecord("ab", nullptr);
	  TTabRecord tab_record2 = TTabRecord("cd", nullptr);
	
	  EXPECT_EQ(tab_record1 < tab_record2, true);
	}
	
	TEST(BakalinKazakov_TTabRecord, Can_Compare_For_More) {
	  TTabRecord tab_record1 = TTabRecord("cd", nullptr);
	  TTabRecord tab_record2 = TTabRecord("ab", nullptr);
	
	  EXPECT_EQ(tab_record1 > tab_record2, true);
	}

### 2.2 Абстрактный базовый класс, содержит спецификации методов таблицы

Заголовочный файл - *TTable.h*

	#pragma once

	#include "TDatValue.h"
	
	#include <string>
	#include <iostream>
	
	// Абстрактный базовый класс, содержащий спецификацию методов таблицы
	class TTable {
	 public:
	  enum Position {
	    FIRST_POS,
	    CURRENT_POS,
	    LAST_POS
	  };
	
	  TTable(void) : entries_amount_(0), efficiency_(0) {}
	  virtual ~TTable(void) {}
	
	  int GetEntriesAmount(void) const { return entries_amount_; }
	  int GetEfficiency(void) const { return efficiency_; }
	
	  bool IsEmpty(void) const { return entries_amount_ == 0; }
	  virtual bool IsFull(void) const = 0;
	
	  virtual std::string GetKey(Position mode = Position::CURRENT_POS) const = 0;
	  virtual TDatValue* GetValue(Position mode = Position::CURRENT_POS) const = 0;
	
	  virtual TDatValue* FindRecord(std::string key) = 0;
	  virtual void InsRecord(std::string key, TDatValue* value) = 0;
	  virtual void DelRecord(std::string key) = 0;
	
	  virtual void Reset(void) = 0;
	  virtual bool IsTabEnded(void) const = 0;
	  virtual void GoNext(void) = 0;
	
	 protected:
	  int entries_amount_;  // количество записей, но не размер таблицы
	  int efficiency_;  // показатель эффективности выполнения операции
	};

### 2.3 Абстрактный базовый класс для таблиц с непрерывной памятью

Заголовочный файл - *TArrayTable.h*

	#pragma once

	#include "TTable.h"
	#include "TTabRecord.h"
	
	#include <string>
	
	// Абстрактный класс для таблиц с непрерывной памятью
	// для управления структурой хранения
	class TArrayTable : public TTable {
	 public:
	  static const int kTabDefaultSize = 25;
	  static const int kNoEntriesPos = -1;
	
	  explicit TArrayTable(int size = kTabDefaultSize);
	  ~TArrayTable(void);
	
	  virtual bool IsFull(void) const override;
	  int GetTableSize(void) const;
	
	  int GetCurrentPos(void) const;
	  virtual void SetCurrentPos(const int pos);
	
	  virtual std::string GetKey(Position mode = Position::CURRENT_POS)
	  const override;
	  virtual TDatValue* GetValue(Position mode = Position::CURRENT_POS)
	  const override;
	
	  virtual void Reset(void) override;
	  virtual bool IsTabEnded(void) const override;
	  virtual void GoNext(void) override;
	
	 protected:
	  TTabRecord** records_;
	  int size_;
	  int curr_pos_;
	
	  friend TSortTable;
	};

Реализация - *TArrayTable.cpp*

	#include "include/TArrayTable.h"

	#include <string>
	
	TArrayTable::TArrayTable(int size) {
	  if (size <= 0) {
	    throw 1;
	  }
	
	  records_ = new TTabRecord*[size];
	  for (int i = 0; i < size; i++) {
	    records_[i] = nullptr;
	  }
	
	  size_ = size;
	  curr_pos_ = 0;
	}
	
	TArrayTable::~TArrayTable() {
	  for (int i = 0; i < size_; i++) {
	    if (records_[i] != nullptr)
	      delete records_[i];
	  }
	
	  delete[] records_;
	}
	
	bool TArrayTable::IsFull() const {
	  return entries_amount_ >= size_;
	}
	
	int TArrayTable::GetTableSize() const {
	  return size_;
	}
	
	int TArrayTable::GetCurrentPos() const {
	  return curr_pos_;
	}
	
	void TArrayTable::SetCurrentPos(const int pos) {
	  curr_pos_ = ((pos > TArrayTable::kNoEntriesPos)
	                && (pos < entries_amount_)) ? pos : 0;
	}
	
	std::string TArrayTable::GetKey(Position mode) const {
	  int pos = TArrayTable::kNoEntriesPos;
	
	  if (!IsEmpty()) {
	    switch (mode) {
	      case Position::FIRST_POS:
	        pos = 0;
	        break;
	
	      case Position::CURRENT_POS:
	        pos = curr_pos_;
	        break;
	
	      case Position::LAST_POS:
	        pos = entries_amount_ - 1;
	        break;
	    }
	  }
	
	  return (pos == TArrayTable::kNoEntriesPos)
	          ? std::string("") : records_[pos]->key_;
	}
	
	TDatValue* TArrayTable::GetValue(Position mode) const {
	  int pos = TArrayTable::kNoEntriesPos;
	
	  if (!IsEmpty()) {
	    switch (mode) {
	      case Position::FIRST_POS:
	        pos = 0;
	        break;
	
	      case Position::CURRENT_POS:
	        pos = curr_pos_;
	        break;
	
	      case Position::LAST_POS:
	        pos = entries_amount_ - 1;
	        break;
	    }
	  }
	
	  return (pos == TArrayTable::kNoEntriesPos) ? nullptr : records_[pos]->value_;
	}
	
	void TArrayTable::Reset() {
	  curr_pos_ = 0;
	}
	
	bool TArrayTable::IsTabEnded() const {
	  return curr_pos_ == entries_amount_;
	}
	
	void TArrayTable::GoNext() {
	  if (!IsTabEnded())
	    curr_pos_++;
	}

###2.4 Модуль с классом, обеспечивающим реализацию просматриваемых таблиц

Заголовочный файл - *TScanTable.h*

	#pragma once
	
	#include "TArrayTable.h"
	
	#include <string>
	
	// Класс просматриваемых таблиц
	class TScanTable : public TArrayTable {
	 public:
	  TScanTable(int size = TArrayTable::kTabDefaultSize);
	
	  virtual TDatValue* FindRecord(const std::string& key) override;
	  virtual void InsRecord(const std::string& key, TDatValue* value) override;
	  virtual void DelRecord(const std::string& key) override;
	};

Реализация - *TScanTable.cpp*

	#include "include/TScanTable.h"

	#include <string>
	
	TScanTable::TScanTable(int size) : TArrayTable(size) {}
	
	TDatValue* TScanTable::FindRecord(const std::string& key) {
	  int i;
	
	  for (i = 0; i < entries_amount_; i++) {
	    if (records_[i]->key_ == key)
	      break;
	  }
	  efficiency_ = i + 1;
	
	  if (i < entries_amount_) {
	    curr_pos_ = i;
	    return records_[i]->value_;
	  }
	
	  return nullptr;
	}
	
	void TScanTable::InsRecord(const std::string& key, TDatValue* value) {
	  int i;
	
	  for (i = 0; i < size_; i++) {
	    if (records_[i] == nullptr) {
	      records_[i] = new TTabRecord(key, value);
	      entries_amount_++;
	      break;
	    }
	  }
	  efficiency_ = i + 1;
	}
	
	void TScanTable::DelRecord(const std::string& key) {
	  int i;
	
	  for (i = 0; i < size_; i++) {
	    if (records_[i] != nullptr)
	      if (records_[i]->GetKey() == key) {
	        delete records_[i];
	        records_[i] = records_[entries_amount_ - 1];
	        records_[entries_amount_ - 1] = nullptr;
	        entries_amount_--;
	        break;
	      }
	  }
	  efficiency_ = i + 1;
	}

Модульное тестирование - *test_tscantable.cpp*

	TEST(BakalinKazakov_TScanTable, Can_Create_Scan_Table) {
	  ASSERT_NO_THROW(TScanTable scan_table());
	}
	
	TEST(BakalinKazakov_TScanTable, Cant_Create_Scan_Table_With_Negative_Size) {
	  ASSERT_ANY_THROW(TScanTable scan_table(-14));
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Insert_Record) {
	  TScanTable scan_table(5);
	  TWord *word = new TWord("Multiprogramm");
	
	  ASSERT_NO_THROW(scan_table.InsRecord("ab", word));
	}
	
	TEST(BakalinKazakov_TScanTable, IsFull_Works_Correctly) {
	  TScanTable scan_table(3);
	  TWord *word = new TWord("Multiprogramm");
	
	  scan_table.InsRecord("aa", word);
	  scan_table.InsRecord("ab", word);
	  scan_table.InsRecord("ac", word);
	
	  EXPECT_EQ(scan_table.IsFull(), true);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Get_Table_Size) {
	  TScanTable scan_table(3);
	
	  EXPECT_EQ(scan_table.GetTableSize(), 3);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Get_Current_Position) {
	  TScanTable scan_table(3);
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 0);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Set_Current_Position) {
	  TScanTable scan_table(3);
	  TWord *word = new TWord("Multiprogramm");
	  scan_table.InsRecord("aa", word);
	  scan_table.InsRecord("ab", word);
	
	  scan_table.SetCurrentPos(1);
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 1);
	}
	
	TEST(BakalinKazakov_TScanTable, Cant_Set_Current_Position_If_Tab_Is_Empty){
	  TScanTable scan_table(3);
	
	  scan_table.SetCurrentPos(1);
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 0);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Go_Next) {
	  TScanTable scan_table(3);
	  TWord *word = new TWord("Multiprogramm");
	  scan_table.InsRecord("aa", word);
	
	  scan_table.Reset();
	  scan_table.GoNext();
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 1);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Reset_Table) {
	  TScanTable scan_table(3);
	
	  scan_table.GoNext();
	  scan_table.Reset();
	
	  EXPECT_EQ(scan_table.GetCurrentPos(), 0);
	}
	
	TEST(BakalinKazakov_TScanTable, IsTabEnded_Works_Correctly) {
	  TScanTable scan_table(3);
	
	  scan_table.GoNext();
	  scan_table.GoNext();
	  scan_table.GoNext();
	
	  EXPECT_EQ(scan_table.IsTabEnded(), true);
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Find_Record) {
	  TScanTable scan_table(3);
	  TWord *word1 = new TWord("Multiprogramm");
	  TWord *word2 = new TWord("SGBash");
	  scan_table.InsRecord("aa", word1);
	  scan_table.InsRecord("ab", word2);
	
	  TWord* result = (TWord*)scan_table.FindRecord("ab");
	
	  EXPECT_EQ(result->GetWord(), "SGBash");
	}
	
	TEST(BakalinKazakov_TScanTable, Can_Delete_Record) {
	  TScanTable scan_table(3);
	  TWord *word1 = new TWord("Multiprogramm");
	  TWord *word2 = new TWord("SGBash");
	  scan_table.InsRecord("aa", word1);
	  scan_table.InsRecord("ab", word2);
	
	  scan_table.DelRecord("ab");
	  TWord* result = (TWord*)scan_table.FindRecord("ab");
	
	  EXPECT_EQ(result, nullptr);
	}

###2.5 Модуль с классом, обеспечивающим реализацию упорядоченных таблиц

###2.6 Модуль с абстрактным базовым классом объектов-значений для деревьев

Заголовочный файл - *TTreeNode.h*

	#pragma once
	
	#include "include/TTabRecord.h"
	
	#include <string>
	
	// Абстрактный базовый класс объектов-значений для деревьев
	class TTreeNode : public TTabRecord {
	 public:
	  TTreeNode(const std::string key = "", TDatValue* value = nullptr,
	  TTreeNode* left = nullptr, TTreeNode* right = nullptr);
	
	  TTreeNode* GetLeft(void) const;
	  TTreeNode* GetRight(void) const;
	
	  virtual TDatValue* GetCopy(void) override;
	
	 protected:
	  TTreeNode *node_left_, *node_right_;
	
	  friend class TTreeTable;
	  friend class TBalanceTree;
	};

Реализация - *TTreeNode.cpp*

	#include "include/TTreeNode.h"
	
	#include <string>
	
	TTreeNode::TTreeNode(const std::string key, TDatValue* value,
	  TTreeNode* left, TTreeNode* right) :
	  TTabRecord(key, value), node_left_(left), node_right_(right) {}
	
	TTreeNode* TTreeNode::GetLeft() const {
	  return node_left_;
	}
	
	TTreeNode* TTreeNode::GetRight() const {
	  return node_right_;
	}
	
	TDatValue* TTreeNode::GetCopy() {
	  return new TTreeNode(key_, value_, nullptr, nullptr);
	}

###2.7 Модуль с классом, реализующим таблицы в виде деревьев поиска

Заголовочный файл - *TTreeTable.h*

Реализация - *TTreeTable.cpp*

###2.8 Модуль с базовым классом объектов-значений для сбалансированных деревьев

Заголовочный файл - *TBalanceNode.h*

Реализация - *TBalanceNode.cpp*

###2.9 Модуль с классом, реализующим таблицы в виде сбалансированных деревьев поиска

Заголовочный файл - *TBalanceTree.h*

Реализация - *TBalanceTree.cpp*

###2.10 Результаты модульного тестирования

##3. Вывод