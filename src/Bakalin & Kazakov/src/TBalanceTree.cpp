#include "include/TBalanceTree.h"

#include <string>

TBalanceTree::TBalanceTree(void) : TTreeTable() {}

void TBalanceTree::InsRecord(const std::string& key, TDatValue* value) {
  if (IsFull())
    throw std::string("Table is full.");

  InsBalanceTree((TBalanceNode*)node_root_, key, value);
}

void TBalanceTree::DelRecord(const std::string& key) {
  // TODO
}

int TBalanceTree::InsBalanceTree(TBalanceNode*& node, const std::string& key,
TDatValue* value) {
  int height_index = 0;

  if (node == nullptr) {  // вставка вершины
    node = new TBalanceNode(key, value);
    height_index = 1;
    records_amount_++;
  } else if (key < node->key_) {
    // После вставки высота левого поддерева увеличилась => балансировка
    if (InsBalanceTree(TBalanceNode*(node->node_left_), key, value) == 1)
      height_index = LeftTreeBalancing(node);
  } else if (key < node->key_) {
    if (InsBalanceTree(TBalanceNode*(node->node_right_), key, value) == 1)
      height_index = RightTreeBalancing(node);
  } else {
    height_index = 0;
  }

  return height_index;
}

int TBalanceTree::LeftTreeBalancing(TBalanceNode*& node) {
  int height_index = 0;

  // Проверка предыдущей балансировки
  switch(node->balance_) {
    // В поддереве был перевес справа - устанавливается равновесие
    case TBalanceNode::Balance::BALANCE_RIGHT:
      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
      height_index = 0;
      break;

    // В поддереве было равновесие - устанавливается перевес слева
    case TBalanceNode::Balance::BALANCE_OK:
      node->SetBalance(TBalanceNode::Balance::BALANCE_LEFT);
      height_index = 1;
      break;

    // В поддереве был перевес слева - необходима балансировка
    case TBalanceNode::Balance::BALANCE_LEFT:
      TBalanceNode* node1, node2;

      node1 = TBalanceNode*(node->node_left_);

      // Случай 1 - однократный LL-поворот
      if (node1->balance_ == TBalanceNode::Balance::BALANCE_LEFT) {
        node->node_left_ = p1->node_right_;
        p1->node_right_ = node;
        node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
        node = p1;
      // Случай 2 - двукратный LR-поворот
      } else {
        p2 = TBalanceNode*(p1->node_right_);
        p1->node_right_ = p2->node_left_;
        P2->node_left_ = p1;
        node->node_left_ = p2->node_right_;
        p2->node_right_ = node;

        if (p2->balance_ == TBalanceNode::Balance::BALANCE_LEFT) {
          node->SetBalance(TBalanceNode::Balance::BALANCE_RIGHT);
        } else if (p2->balance_ == TBalanceNode::Balance::BALANCE_RIGHT) {
          p1->SetBalance(TBalanceNode::Balance::BALANCE_LEFT);
        } else {
          p1->SetBalance(TBalanceNode::Balance::BALANCE_OK);
        }

        node = p2;
      }

      node->SetBalance(TBalanceNode::Balance::BALANCE_OK);
      height_index = 0;
      break;
  }  // switch

  return height_index;
}

int TBalanceTree::RightTreeBalancing(TBalanceNode*& node) {
  // TODO
}
