#include "include/TScanTable.h"

#include <string>

TScanTable::TScanTable(const int size) : TArrayTable(size) {}

TDatValue* TScanTable::FindRecord(const std::string& key) {
  int i;

  for (i = 0; i < records_amount_; i++) {
    if (records_[i]->key_ == key)
      break;
  }
  efficiency_ = i + 1;

  if (i < records_amount_) {
    curr_pos_ = i;
    return records_[i]->value_;
  }

  return nullptr;
}

void TScanTable::InsRecord(const std::string& key, TDatValue* value) {
  int i;

  for (i = 0; i < size_; i++) {
    if (records_[i] == nullptr) {
      records_[i] = new TTabRecord(key, value);
      records_amount_++;
      break;
    }
  }
  efficiency_ = i + 1;
}

void TScanTable::DelRecord(const std::string& key) {
  int i;

  for (i = 0; i < size_; i++) {
    if (records_[i] != nullptr)
      if (records_[i]->GetKey() == key) {
        delete records_[i];
        records_[i] = records_[records_amount_ - 1];
        records_[records_amount_ - 1] = nullptr;
        records_amount_--;
        break;
      }
  }
  efficiency_ = i + 1;
}
