#include "include/TBalanceNode.h"

#include <string>

TBalanceNode::TBalanceNode(const std::string key, TDatValue* value,
  TTreeNode* left, TTreeNode* right, Balance balance) :
  TTreeNode(key, value, left, right), balance_(balance) {}

TBalanceNode::Balance TBalanceNode::GetBalance() const {
  return balance_;
}

void TBalanceNode::SetBalance(TBalanceNode::Balance balance) {
  balance_ = balance;
}

TDatValue* TBalanceNode::GetCopy() {
  return new TBalanceNode(key_, value_, nullptr, nullptr, Balance::BALANCE_OK);
}
