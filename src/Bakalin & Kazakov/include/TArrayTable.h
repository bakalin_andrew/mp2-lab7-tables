#pragma once

#include "include/TTable.h"
#include "include/TTabRecord.h"

#include <string>

// Абстрактный класс для таблиц с непрерывной памятью
// для управления структурой хранения
class TArrayTable : public TTable {
 public:
  static const int kTabDefaultSize = 25;
  static const int kNoRecordsPos = -1;

  explicit TArrayTable(const int size = kTabDefaultSize);
  ~TArrayTable(void);

  virtual bool IsFull(void) const override;
  int GetTableSize(void) const;

  int GetCurrentPos(void) const;
  virtual void SetCurrentPos(const int pos);

  virtual std::string GetKey(Position mode = Position::CURRENT_POS)
  const override;
  virtual TDatValue* GetValue(Position mode = Position::CURRENT_POS)
  const override;

  virtual void Reset(void) override;
  virtual bool IsTabEnded(void) const override;
  virtual void GoNext(void) override;

 protected:
  TTabRecord** records_;
  int size_;
  int curr_pos_;

  friend TSortTable;
};
