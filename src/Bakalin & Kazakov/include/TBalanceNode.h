#pragma once

#include "include/TTreeNode.h"

#include <string>

// Базовый класс объектов-значений для сбалансированных деревьев
class TBalanceNode : public TTreeNode {
 public:
  enum Balance {
    BALANCE_OK,
    BALANCE_LEFT,
    BALANCE_RIGHT
  };

  TBalanceNode(const std::string key = "", TDatValue* value = nullptr,
  TTreeNode* left = nullptr, TTreeNode* right = nullptr,
  Balance balance = Balance::BALANCE_OK);

  Balance GetBalance(void) const;
  void SetBalance(Balance balance);

  virtual TDatValue* GetCopy(void) override;

 protected:
  Balance balance_;  // индекс балансировки вершины

  friend class TBalanceTree;
};
