#pragma once

#include "include/TTreeTable.h"
#include "include/TBalanceNode.h"

#include <string>

// Класс для сбалансированных деревьев (TBalanceTree).
class TBalanceTree : public TTreeTable  {
 public:
  TBalanceTree(void);

  virtual void InsRecord(const std::string& key, TDatValue* value) override;
  virtual void DelRecord(const std::string& key) override;

 protected:
  int InsBalanceTree(TBalanceNode*& node, const std::string& key,
  TDatValue* value);

  int LeftTreeBalancing(TBalanceNode*&);
  int RightTreeBalancing(TBalanceNode*&);
};
