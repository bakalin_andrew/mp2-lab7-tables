#pragma once

#include "include/TDatValue.h"

#include <string>

// Класс объектов-значений для записей таблицы
class TTabRecord : public TDatValue {
 public:
  TTabRecord(const std::string key = "", TDatValue* value = nullptr);
  TTabRecord& operator=(TTabRecord&);

  void SetKey(const std::string& key);
  std::string GetKey(void) const;
  void SetValue(TDatValue* value);
  TDatValue* GetValue(void) const;

  virtual TDatValue* GetCopy(void) override;

  virtual bool operator==(const TTabRecord&) const;
  virtual bool operator<(const TTabRecord&) const;
  virtual bool operator>(const TTabRecord&) const;

 protected:
  std::string key_;  // ключ записи
  TDatValue* value_;

  // Дружественные классы для различных типов таблиц
  friend class TArrayTable;
  friend class TScanTable;
  friend class TSortTable;
  friend class TTreeNode;
  friend class TTreeTable;
  friend class TArrayHash;
  friend class TListHash;
};
