#pragma once

#include "include/TTreeNode.h"
#include "include/TTable.h"

#include <stack>
#include <string>

// Таблицы со структурой хранения в виде деревьев поиска
class TTreeTable: public TTable {
 public:
  TTreeTable(void);
  ~TTreeTable(void);

  virtual bool IsFull(void) const override;

  virtual std::string GetKey(Position mode = Position::CURRENT_POS) const override;
  virtual TDatValue* GetValue(Position mode = Position::CURRENT_POS) const override;

  virtual TDatValue* FindRecord(const std::string& key) override;
  virtual void InsRecord(const std::string& key, TDatValue* value) override;
  virtual void DelRecord(const std::string& key) override;

  virtual void Reset(void) override;
  virtual bool IsTabEnded(void) const override;
  virtual void GoNext(void) override;

 protected:
  TTreeNode *node_root_;
  TTreeNode *node_current_;

  // Адрес указателя на вершину результата в FindRecord
  TTreeNode **node_found_;

  int curr_pos_;  // номер текущей вершины
  std::stack<TTreeNode*> iterator_stack_;  // стек для итератора

  static void DeleteTreeTable(TTreeNode*);
};
